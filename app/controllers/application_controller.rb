require 'jwt'

class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :add_allow_credentials_headers
  before_action :verificar_token, only: [:listar_categoria_por_usuario, :remover_categoria, :listar_boletos_por_dia, :salvar_boleto]

  attr_reader :usuario

  def index
    render 'index'
  end

  def add_allow_credentials_headers
    response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] || '*'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS'
  end

  def gerar_token(usuario)
    exp = 24.hours.from_now
    usuario_json = usuario.as_json(only: [:id, :email])
    exp_payload = {:data => usuario_json, :exp => exp.to_i}
    token = JWT.encode exp_payload, senha_secreta, HMAC
    return token
  end

  def verificar_token
    token = request.headers['x-access-token']

    if token.nil? || token.to_s.empty?
      render json: Response.new(false, "É necessário informar token!", nil)
      return
    end

    senha_token = senha_secreta
    begin
      decoded_token = JWT.decode(token, senha_token, true, {:algorithm => HMAC}).first
      data = decoded_token.fetch('data')
      usuario_id = data.fetch('id')
      @usuario = Usuario.where("id = ?", usuario_id).first
    rescue JWT::ImmatureSignature => im
      render json: Response.new(false, "#{im.message}", nil)
    rescue JWT::ExpiredSignature => es
      render json: Response.new(false, "#{es.message}", nil)
    rescue JWT::DecodeError => de
      render json: Response.new(false, "#{de.message}", nil)
    end
  end

  def senha_secreta
    'coneCt@$ol'
  end

  HMAC = 'HS256'
end
