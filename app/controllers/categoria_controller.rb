require 'json'

class CategoriaController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def listar_categoria_por_usuario
    id = params[:id_usuario]
    categorias = Categoria.where("usuario_id = ?", id)
    render json: Response.new(true, "", categorias.as_json)
  end

  def salvar_categoria
    categoria_json = params[:categoria]
    token = params[:token]
    categoria = Categoria.new
    categoria = categoria.converter_json categoria_json
    if categoria.save
      render json: Response.new(true, "Salvo com sucesso", nil)
    else
      render json: Response.new(false, "Não foi possivel salvar categoria", nil)
    end
  end


  def remover_categoria
    Categoria.find(4).destroy
  end

end