require 'json'

class BoletoController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def listar_boletos_por_dia
    dataStr = params[:dataStr]
    boletos = Boleto.where("DATE(data_vencimento) = ?", Date.parse(dataStr))

    if usuario
      render json: Response.new(true, "", boletos)
    else
      render json: Response.new(false, "Token", nil)
    end
  end

  def listar
    render json: Response.new(true, "", Boleto.all)
  end

  def salvar_boleto
    boleto_json = params[:boleto]
    boleto = Boleto.new
    boleto = boleto.converter_json boleto_json
    if boleto.save
      render json: Response.new(true, "", nil)
    else
      render json: Response.new(false, "Não foi possivel salvar o boleto", nil)
    end
  end
end