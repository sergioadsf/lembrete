class UsuarioController < ApplicationController

  skip_before_filter :verify_authenticity_token

  def logar
    login = params[:login]
    senha = params[:senha]

    usuario = Usuario.where("email = ? and senha = ?", login, senha)

    if usuario.exists?
      usuario = usuario.first
      response = Response.new(true, "", usuario)
      token = gerar_token(usuario)
      usuario.token = token
      usuario.atualizar_token
      render json: response
    else
      render json: Response.new(false, "Login ou senha não conferem", nil)
    end
  end

  def cadastrar
    usuario = Usuario.new

    usuario.nome = params[:nome]
    usuario.senha = params[:senha]
    usuario.email = params[:email]

    if usuario.save
      render json: Response.new(true, "", usuario)
    else
      render json: Response.new(false, "Não foi possivel salvar o usuário", nil)
    end

  end

  def listar
    page = params[:page]
    token = params[:token]
    quantidade = 5
    render json: Usuario.limit(quantidade).offset(page.to_i * quantidade).order(:id)
  end

  def salvar_gcm
    id = params[:id]
    gcm = params[:gcm]

    usuario = Usuario.where("id = ?", id).first
    usuario.gcm = gcm
    if usuario.atualizar_gcm
      render json: Response.new(true, "", nil)
    else
      render json: Response.new(false, "Login ou senha não conferem", nil)
    end
  end

  def teste
    ::GcmUtil::send Usuario.where("gcm is not null"), "teste", Boleto.find(1)
    render json: Response.new(false, Date.tomorrow, nil)
  end
end
