class Boleto < ActiveRecord::Base

	belongs_to :usuario


	def listarPorData(dataStr) 
		Boleto.where("DATE(data_vencimento) = ?", Date.parse(dataStr))
	end

	def listarPorUsuario(id) 
		Boleto.where("usuario_id = ?", id)
	end

	def listar
		all
	end

	def listar_boletos_1__dia_a_vencer
		Boleto.where("DATE(data_vencimento) = ?", Date.tomorrow)
	end

	def enviar_boletos_1_dia_a_vencer
		Thread.new {
			begin
				boletos = listar_boletos_1__dia_a_vencer
				puts "okBol #{boletos.size}"
				if boletos.size > 0
					::GcmUtil::send Usuario.where("gcm is not null"), "teste", "Aqui"
					puts "ok passou"
				end
			ensure
				ActiveRecord::Base.connection_pool.release_connection
			end
		}
	end

	def converter_json(boleto_json)
		boleto_json = JSON.parse(boleto_json)
		self.descricao = boleto_json['descricao'] 
		self.codigo_barras = boleto_json["codigoBarras"] 
		self.data_pagamento = boleto_json["dataPagamento"] 
		self.data_vencimento = boleto_json["dataVencimento"] 
		self.valor = boleto_json["valor"] 
		self.lembrar = boleto_json["lembrar"] 
		self.usuario_id = boleto_json["usuarioId"] 
		self.categoria_id = boleto_json["categoriaId"] 
		self
	end

end
