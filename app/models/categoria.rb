class Categoria < ActiveRecord::Base
	has_many :boletos
	belongs_to :usuario

	def converter_json(categoria_json)
		categoria_json = JSON.parse(categoria_json)
		self.descricao = categoria_json['descricao'] 
		self.nome = categoria_json["nome"] 
		self.usuario_id = categoria_json["usuarioId"] 
		self
	end 
end
