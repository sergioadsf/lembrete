class Response 

	attr_reader :sucesso, :mensagem, :objetoRetorno

	def initialize(sucesso, mensagem, objetoRetorno)
		@sucesso = sucesso
		@mensagem = mensagem
		@objetoRetorno = objetoRetorno
	end

end