class Usuario < ActiveRecord::Base
	has_many :boletos

	validates :nome, :email, :senha, presence: true

	def listar 

		Thread.new {
			begin
				puts Usuario.all
			ensure
				ActiveRecord::Base.connection_pool.release_connection
			end
		}
	end

	def atualizar_token
		update(token: token)
	end

	def atualizar_gcm
		update(gcm: gcm)
	end

end
