Rails.application.routes.draw do

	post "/ws/usuarios/logar" => "usuario#logar"
	post "/ws/usuarios/cadastrar" => "usuario#cadastrar"
	post "/ws/usuarios/salvarGCM" => "usuario#salvar_gcm"
	get "/ws/usuarios/listar" => "usuario#listar"
	get "/ws/usuarios/encode" => "usuario#encode"
	get "/ws/usuarios/decode" => "usuario#decode"
	get "/ws/usuarios/teste" => "usuario#teste"


	post "/ws/boletos/salvar" => "boleto#salvar_boleto"
	post "/ws/boletos/listar" => "boleto#listar"
	post "/ws/boletos/listarPorDia" => "boleto#listar_boletos_por_dia"

	post "/ws/categorias/listar" => "categoria#listar"
	post "/ws/categorias/listarPorUsuario" => "categoria#listar_categoria_por_usuario"
	post "/ws/categorias/salvar" => "categoria#salvar_categoria"
	post "/ws/categorias/remove" => "categoria#remover_categoria"
end
