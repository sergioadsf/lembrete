require 'gcm'

class GcmUtil 

	# @@gcm = GCM.new("AIzaSyDD2bYXyrVq8Sr-OqzZfYfglEvGmZ-lMa0")
	@@gcm = GCM.new("AIzaSyA1mPZXgmUl_ylO9NJxSlN-eMO4RVpSCys")

	def self.send(usuarios, title, value)
		content = Content.new(usuarios, title, value)
		response = @@gcm.send(content.registration_ids, content.options)
	end

end
class Content
	attr_reader :options, :registration_ids

	def initialize(usuarios, title, value)
		@registration_ids = []
		usuarios.each do |user|
			@registration_ids << user.gcm
		end
		@options = Option.datas title, value
	end

end

class Option
	def self.datas(title, value)
		data = {data: {title: title, value: value}, collapse_key: "updated_score"}
		data
	end

end
