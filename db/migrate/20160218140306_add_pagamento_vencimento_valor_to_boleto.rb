class AddPagamentoVencimentoValorToBoleto < ActiveRecord::Migration
  def change
    add_column :boletos, :valor, :double
    add_column :boletos, :data_pagamento, :date
    add_column :boletos, :data_vencimento, :date
  end
end
