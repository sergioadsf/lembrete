class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nome
      t.string :cidade
      t.string :estado
      t.string :pais

      t.timestamps
    end
  end
end
