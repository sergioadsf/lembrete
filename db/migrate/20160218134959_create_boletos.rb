class CreateBoletos < ActiveRecord::Migration
  def change
    create_table :boletos do |t|
      t.text :descricaco
      t.string :codigo_barras

      t.timestamps
    end
  end
end
