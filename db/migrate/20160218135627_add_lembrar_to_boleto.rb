class AddLembrarToBoleto < ActiveRecord::Migration
  def change
    add_column :boletos, :lembrar, :boolean
    rename_column :boletos, :descricaco, :descricao
  end
end
